import com.google.cloud.dialogflow.cx.v3.AgentName;
import com.google.cloud.dialogflow.cx.v3.EntityType;
import com.google.cloud.dialogflow.cx.v3.EntityTypesClient;
import com.google.cloud.dialogflow.cx.v3.EntityTypesSettings;
import com.google.cloud.dialogflow.cx.v3.UpdateEntityTypeRequest;
import com.google.protobuf.FieldMask;

import java.io.IOException;
import java.util.List;

// To use com.google.cloud.dialogflow.cx.v3 library, GOOGLE_APPLICATION_CREDENTIALS environment variable needs to be set.
// EntityType.name is unique identifier of EntityType.
// EntityType.displayName is the name that is given (i.e. "category", "brand").

public class EntityTypeManager {
    private static final String LOCATION_GLOBAL_SERVICE_ENDPOINT = "dialogflow.googleapis.com:443";
    private static final String FIELD_MASK_ENTITIES = "entities";

    // These values depend on the Dialogflow project.
    private static final String PROJECT_ID = "cxbottesting";
    private static final String LOCATION_ID = "global";
    private static final String AGENT_ID = "ea1ef0c9-0863-4846-a0f3-7a8c173a597a";

    public static void createEntityType(String displayName) throws IOException {
        try (EntityTypesClient client = EntityTypesClient.create(generateEntityTypesSettings())) {
            EntityType entityType = EntityType.newBuilder().setDisplayName(displayName).setKind(EntityType.Kind.KIND_MAP).build();
            client.createEntityType(generateAgentName(), entityType);
        }
    }

    public static void deleteEntityType(String displayName) throws IOException {
        try (EntityTypesClient client = EntityTypesClient.create(generateEntityTypesSettings())) {
            client.deleteEntityType(getEntityTypeByDisplayName(displayName).getName());
        }
    }

    public static void updateEntitiesInEntityType(String displayName, List<EntityType.Entity> entities) throws IOException {
        try (EntityTypesClient client = EntityTypesClient.create(generateEntityTypesSettings())) {
            EntityType entityTypeToUpdate = getEntityTypeByDisplayName(displayName);
            EntityType updatedEntityType = EntityType.newBuilder(entityTypeToUpdate).clearEntities().addAllEntities(entities).build();
            FieldMask fieldMask = FieldMask.newBuilder().addPaths(FIELD_MASK_ENTITIES).build();

            UpdateEntityTypeRequest request =
                UpdateEntityTypeRequest.newBuilder().setEntityType(updatedEntityType).setUpdateMask(fieldMask).build();
            client.updateEntityType(request);
        }
    }

    public static EntityType.Entity generateEntity(String entityName, List<String> synonyms) {
        return EntityType.Entity.newBuilder().setValue(entityName).addAllSynonyms(synonyms).build();
    }

    private static EntityType getEntityTypeByDisplayName(String displayName) throws IOException {
        try (EntityTypesClient client = EntityTypesClient.create(generateEntityTypesSettings())) {
            List<EntityType> entityTypes = client.listEntityTypes(generateAgentName()).getPage().getResponse().getEntityTypesList();
            return entityTypes.stream().filter(entityType -> displayName.equals(entityType.getDisplayName())).findFirst().get();
        }
    }

    private static EntityTypesSettings generateEntityTypesSettings() throws IOException {
        return EntityTypesSettings.newBuilder().setEndpoint(LOCATION_GLOBAL_SERVICE_ENDPOINT).build();
    }

    private static AgentName generateAgentName() {
        return AgentName.of(PROJECT_ID, LOCATION_ID, AGENT_ID);
    }

}
