import com.google.cloud.dialogflow.cx.v3.EntityType.Entity;

import java.util.Arrays;

public class Main {

    private static Entity phoneCategory =
        EntityTypeManager.generateEntity("Phone", Arrays.asList("cellphone", "mobile phone", "telephone", "smartphone"));
    private static Entity laptopCategory = EntityTypeManager.generateEntity("Laptop", Arrays.asList("notebook", "ultrabook"));
    private static Entity appleBrand = EntityTypeManager.generateEntity("Apple", Arrays.asList("apple"));
    private static Entity dellBrand = EntityTypeManager.generateEntity("Dell", Arrays.asList("dell"));

    public static void main(String[] args) throws Exception {

        EntityTypeManager.createEntityType("category");
        EntityTypeManager.createEntityType("brand");

        EntityTypeManager.updateEntitiesInEntityType("category", Arrays.asList(phoneCategory, laptopCategory));
        EntityTypeManager.updateEntitiesInEntityType("brand", Arrays.asList(appleBrand, dellBrand));

        EntityTypeManager.deleteEntityType("category");
        EntityTypeManager.deleteEntityType("brand");
    }
}
